﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UCSE_FINAL_PROG1_JULIO_2020_PRON_MARCOS
{
    public class Rodilla:ORTESIS
    {
        public double LargoTotal { get; set; }
        public bool SoporteParaRotula { get; set; }
        public double DiametroInferior { get; set; }
        public double DiametroSuperior { get; set; }

        public override int Recargo()
        {
            if (SoporteParaRotula==true)
            {
                return base.Recargo()+4;
            }
            return base.Recargo();
        }
        public override string descripcionPersonalizada()
        {
            if (SoporteParaRotula==true)
            {
                return "Tiene Soporte Para Rotula";
            }
            return "No tiene Soporte Para ROTULA";
        }
    }
}
