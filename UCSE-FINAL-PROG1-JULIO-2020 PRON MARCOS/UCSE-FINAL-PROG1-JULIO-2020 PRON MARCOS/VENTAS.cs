﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UCSE_FINAL_PROG1_JULIO_2020_PRON_MARCOS
{
    public class VENTAS
    {
        public string CodigoOrtesis { get; set; }
        public int DNICliente { get; set; }

        public DateTime FechaVenta { get; set; }
        public int PorcentajeRecargo { get; set; }
        public double PrecioTotal { get; set; }

    }
}
