﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UCSE_FINAL_PROG1_JULIO_2020_PRON_MARCOS
{
    public enum COLOR
    {
        Blanco, Azul, Verde, Negro
    }
    public enum MATERIAL
    {
        Termoplastico, Venda, Mixto
    }
    public abstract class ORTESIS
    {
        public string CodigoAlfanumerico { get; set; }
        public string Nombre { get; set; }
        public COLOR Color { get; set; }
        public MATERIAL MaterialConfeccion { get; set; }
        public int PrecioUnitario { get; set; }
        public int StockActualizado { get; set; }
        public string Descripcion { get; set; }

        public abstract string descripcionPersonalizada();
        
        public virtual int Recargo()
        {
            if (MaterialConfeccion==MATERIAL.Termoplastico)
            {
                return 3;
            }
            return 0;
        }

    }
}
