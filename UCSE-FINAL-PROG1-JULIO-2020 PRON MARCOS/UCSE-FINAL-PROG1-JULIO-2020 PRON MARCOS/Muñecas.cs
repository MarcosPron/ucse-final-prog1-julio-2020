﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UCSE_FINAL_PROG1_JULIO_2020_PRON_MARCOS
{
    public class Muñecas:ORTESIS
    {
        public bool Universal { get; set; }
        public bool SoportePulgar { get; set; }
        public bool MovimientoActivo { get; set; }

        public override int Recargo()
        {
            if (SoportePulgar==true)
            {
                return base.Recargo() + 3;
            }
            return base.Recargo();
        }
        public override string descripcionPersonalizada()
        {
            if (SoportePulgar == true)
            {
                return "Tiene Soporte Pulgar";
            }
            return "No tiene Soporte Pulgar";
        }
    }
}
