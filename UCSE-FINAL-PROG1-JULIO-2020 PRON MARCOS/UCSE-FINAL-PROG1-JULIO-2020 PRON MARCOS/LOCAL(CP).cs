﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UCSE_FINAL_PROG1_JULIO_2020_PRON_MARCOS
{
    public class LOCAL_CP_
    {
        public List<VENTAS> ListaDeVentas { get; set; }
        public List<CLIENTES> ListaDeClientes { get; set; }
        public List<ORTESIS> ListaOrtesis { get; set; }
        public List<LOCALIDAD> ListaDeLocalidades { get; set; }

        public bool CargarNuevaOrtesis(ORTESIS nuevaortesis)
        {
            ORTESIS objeto = ListaOrtesis.Find(x => x.CodigoAlfanumerico == nuevaortesis.CodigoAlfanumerico);

            //CORRECCIÓN: SI ESTO NO TRAE UN OBJETO, ACA VA A ROMPER POR OBJETO NULO, objeto QUEDA NULO SI ES ASI.
            if (objeto.CodigoAlfanumerico==null)
            {
                return true;
            }

            //CORRECCIÓN: bool.Parse MAL USADO
            return bool.Parse("Este codigo Ya existe");
        }

        //CORRECCIÓN: CODIGO INNECESARIO.
        public int Porcentajetotalrecargo(ORTESIS ortesis)
        {
            return ortesis.Recargo();
        }

        public bool GenerarVenta(string codigoortesis, int dni)
        {
            foreach (var ortesis in ListaOrtesis)
            {
                //CORRECCIÓN: NO SE RESTA AL STOCK!
                if (ortesis.CodigoAlfanumerico==codigoortesis)
                {
                    VENTAS nuevaventa = new VENTAS();
                    nuevaventa.CodigoOrtesis = ortesis.CodigoAlfanumerico;
                    nuevaventa.DNICliente = dni;
                    nuevaventa.FechaVenta = DateTime.Now;
                    nuevaventa.PorcentajeRecargo =ortesis.Recargo();
                    double recargofinal = (ortesis.PrecioUnitario * ortesis.Recargo()) / 100;
                    nuevaventa.PrecioTotal = ortesis.PrecioUnitario + recargofinal;
                    ListaDeVentas.Add(nuevaventa);
                    return true;
                }
            }
            return false;
        }

        public List<CLIENTES> ListaPorProvincia(PROVINCIA provincia)
        {
            List<CLIENTES> NuevaLista = new List<CLIENTES>();
            foreach (var cliente in ListaDeClientes)
            {
                foreach (var localidad in ListaDeLocalidades)
                {
                    if (cliente.CodigoPostal==localidad.CodigoPostal)
                    {
                        if (localidad.Provincia.NombreProvincia==provincia.NombreProvincia)
                        {
                            NuevaLista.Add(cliente);                            
                        }
                    }
                }
            }
            return NuevaLista;
        }

        public List<string> VentasConParametros(DateTime fechaa,DateTime fechab,PROVINCIA provincia)
        {
            List<string> ListaAImprimir = new List<string>();
            foreach (var venta in ListaDeVentas)
            {
                if (venta.FechaVenta>fechaa && venta.FechaVenta<fechab)
                {
                    //CORRECCIÓN: LO CORRECTO ES USAR EL MÉTODO DESARROLLADO ANTERIORMENTE.
                    foreach (var localidad in ListaDeLocalidades)
                    {
                        if (localidad.Provincia.CodigoPorvincia==provincia.CodigoPorvincia)
                        {
                            var ortesis = ListaOrtesis.Find(x => x.CodigoAlfanumerico == venta.CodigoOrtesis);
                            //CORRECCION: EL NOMBRE DE LA ORTESIS DEBE RESOLVERSE EN EL METODO descripcionPersonalizada
                            ListaAImprimir.Add(venta.FechaVenta + " - " + ortesis.Nombre + " - "+ ortesis.descripcionPersonalizada()+ " - "+venta.PrecioTotal);
                        }
                    }
                }
            }
            return ListaAImprimir;
        }
    }
}
