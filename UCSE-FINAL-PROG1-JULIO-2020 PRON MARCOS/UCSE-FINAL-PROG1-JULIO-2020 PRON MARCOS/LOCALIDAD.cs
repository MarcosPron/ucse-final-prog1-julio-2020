﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UCSE_FINAL_PROG1_JULIO_2020_PRON_MARCOS
{
    public class LOCALIDAD
    {
        public int CodigoPostal { get; set; }
        public string Nombre { get; set; }
        public PROVINCIA Provincia { get; set; }
    }
}
