﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UCSE_FINAL_PROG1_JULIO_2020_PRON_MARCOS
{
    public class PieTobillo: ORTESIS
    {
        public int Talle { get; set; }
        public bool AutoAjustable { get; set; }
        public bool MovimientoActivo { get; set; }

        public override string descripcionPersonalizada()
        {
            if (AutoAjustable == true)
            {
                return "Es Auto ajustable";
            }
            return "No es autoajustable";
        }
    }
}
