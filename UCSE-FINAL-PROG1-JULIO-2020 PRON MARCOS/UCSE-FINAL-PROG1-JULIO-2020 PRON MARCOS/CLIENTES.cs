﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UCSE_FINAL_PROG1_JULIO_2020_PRON_MARCOS
{
    public class CLIENTES
    {
        public int DNI { get; set; }
        public string NombreYApellido { get; set; }
        public string Domicilio { get; set; }
        public int CodigoPostal { get; set; }
    }
}
